package com.tngtech.jira.plugins.schizophrenia.exception;

import com.atlassian.jira.user.ApplicationUser;

public class UserNotInPossibleUsersException extends Exception {
    public UserNotInPossibleUsersException(ApplicationUser user) {
        super(String.format("User '%s' is not in the group of possible users to switch to.", user.getUsername()));
    }
}

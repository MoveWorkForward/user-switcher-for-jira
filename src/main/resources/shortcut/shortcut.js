
AJS.toInit(function() {
	var shifter,
		schizophrenia = new Schizophrenia(),
		switchToUserButton = AJS.$('<span id="switch-user-link" style="display: none">Switch To User</span>');
	
	schizophrenia.init(AJS.params.baseURL);
		
	AJS.$('#header').append(switchToUserButton);
	switchToUserButton.click(function() {
		if(AJS.params.loggedInUser || AJS.Meta.get("remote-user")) {
			schizophrenia.mayUserSwitch(function(response) {
                if(response.mayUserSwitch) {
					shifter.show('switch');
					
					AJS.$('#schizophrenia-shifter-dialog .hint-container').html(Schizophrenia.Templates.Shortcut.tip());
					AJS.$('#schizophrenia-shifter-dialog .hint-container').attr('title', AJS.I18n.getText("schizophrenia.keyboard.shortcut.shifter.tip-tooltip"));
					AJS.$('#schizophrenia-shifter-dialog-field').attr({
						placeholder: AJS.I18n.getText("schizophrenia.keyboard.shortcut.shifter.placeholder"),
						value: ''
					});
                }
			});
		}
	});
	
	shifter = new JIRA.ShifterComponent.ShifterController('schizophrenia-shifter-dialog');
	shifter.register(function() {
		var userlist = jQuery.Deferred();
		
		AJS.$.getJSON(schizophrenia.restApiUrl + '/switchuser/possibleusers', function(response) {
			var i, ii,
				user,
				items = [],
				users = response.users;
			
			schizophrenia.sortUsers(users);
			for(i = 0, ii = users.length; i < ii; i++) {
				user = users[i];
				if(user.username !== AJS.params.loggedInUser) {
					items.push({
						label: user.displayName + ' (' + user.username + ')',
						value: user.username
					});
				}
			}

			userlist.resolve(items);
		}).error(function() {
			userlist.reject();
		});
		
		return {
			id: 'switch',
			name: AJS.I18n.getText("schizophrenia.keyboard.shortcut.shifter.name"),
			weight: 600,
			getSuggestions: function() {
				return userlist;
			},
			onSelection: function(username) {
				schizophrenia.switchToUser(username);
			}
		}
	});
});
